/**
 * Enum for level values.
 * @readonly
 * @enum {number}
 *
 * @todo It need's to be UMD friendly, it can't stay like this.
 */
var Level = {
    /** Ignore Objects and Arrays */
    SHALLOW: 0,
    /** Unwrap if an Array; If an Object, create array of values of given @param {(string|regex)}*/
    FLAT: 1,
    /** Unwrap if an Array; If an Object, ignore functions, get only values */
    DEEP: 2
};

/*global window, console, ko, require, exports, module, define, factory*/
/*jslint nomen: true*/
/*jslint todo: true*/
(function (factory) {
    //"use strict";

    // CommonJS/Node
    if (typeof require === "function" && typeof exports === "object" && typeof module === "object") {
        factory(require("ko"),  exports);
    } else if (typeof define === "function" && define.amd) { // AMD module
        define(["knockout", "exports"], factory);
    } else { // No module loader
        factory(ko, ko);
    }
}(function (ko, exports) {
    //"use strict";

    /**
     * Will create a new Dirty Flag to evaluate if observables in a view model are "dirty".
     * @see https://en.wikipedia.org/wiki/Cache_(computing)#Dirty
     * @see http://www.knockmeout.net/2011/05/creating-smart-dirty-flag-in-knockoutjs.html
     *
     * @constructor
     * @params {object}         target               The view model properties to flag/track.
     * @params {boolean}        [startsDirty]        If true, will flag the view model as dirty from the start.
     * @params {object}         options          Defines deepness of dirty flag.
     * @params {Level}          options.level    Denotes deepness of simplification.
     * @params {(string|regex)} options.getParam Defines key that will be used to convert array of objects to array of values.
     * @todo Make use of option.matchingCriteria, option.ignoreComputed, option.ignore
     * #params {(string|regex)}    option.matchingCriteria - It's used to find similar object in array.
     * #params {boolean}           option.ignoreComputed   - Whether changed computed and pureComputed values should be taken into account when comparing objects.
     * #params {(string|string[])} option.ignore           - One or array of properies names to ignore when comparing.
     *
     * @throws {Error} Argument target must be passed.
     * @throws {Error} Argument options must be an Object type.
     * @throws {Error} When Level.FLAT is choosed, there must be getParam specified as well.
     *
     * @example
     * new ko.DirtyFlag(self.categories, false, {level: Level.SHALLOW});
     *
     * @example
     * new ko.DirtyFlag(self.categories, false, {level: Level.FLAT, getParam: "categoryID"});
     *
     * @example
     * new ko.DirtyFlag(self.categories, false, {level: Level.DEEP});
     */

    /* jshint ignore:start */
    exports = ko.DirtyFlag = function () {};
    /* jshint ignore:end */

    /** @section Public API*/
    var DirtyFlag = function (target, startsDirty, options) { //jshint ignore:line

        if (!target) {
            throw new Error("You need to pass a 'target' to flag.");
        }

        if (!(typeof options !== "object" || options !== "undefined")) {
            throw new Error("options must be an object.");
        }

        var self         = this,
        // Set default when options is empty object (or is undefined).
            _level       = !Object.size(options) ? {level: Level.DEEP} : options,
            _cleanTarget = ko.observable(ko.toJSON(target)),
        // Set false as default when startsDirty is not a boolean value (or is undefined).
            _startsDirty = Object.isBoolean(startsDirty) ? ko.observable(startsDirty) : ko.observable(false),
            status;

        status = function () {

            self._cleanTarget_ = _cleanTarget;
            self._target_ = target;
            self._startsDirty_ = _startsDirty;
            // If getParam is specified, ignore given level and set it to 1.
            // But when level is setted to 1, and there's no getParam, return an Error.
            if (_level.level !== Level.FLAT && _level.getParam) {
                self.level = Level.FLAT;
            } else if (_level.level === Level.FLAT && !_level.getParam) {
                throw new Error("If level is FLAT, there must be a getParam specified!");
            } else {
                self.level = _level.level;
            }
            // in case of level 1
            self.param = _level.getParam || "";

            /**
             * It's used to find similar object in array,
             * by reducing number of properties in oryginal object
             * only to those specified in matchingCriteria.
             *
             * @type {(string|Regex)}
             *
             */
            self.matchingCriteria = /\b(?!(FK|fk))\w*(id|Id|ID)\b/;

            /**
             * Gets objects type.
             * This is extended version for knockouts "computed" and "pureComputed" types.
             * @see https://javascriptweblog.wordpress.com/2011/08/08/fixing-the-javascript-typeof-operator/
             *
             * @private
             * @param   {*}         obj         - Any type of data, that will be checed for its data type.
             * @param   {Object}    orygObj     - Any type of data, that will help to check ORYGINAL data type of an property obj.
             * @param   {string}    objKey      - Key of an obj property by which value from orygObj will be fetched.
             * @return  {string}    Name of obj data type.
             *
             * @todo move this function outside of exported API
             * @todo make possible to store black or white list of properties in JSON config file, or to pass them as an option
             */
            self.toType = (function toType() {
                return function (obj, orygObj, objKey) {
                    // Since unwrapping all ko objects using ko.toJS unwrapping "computed" as well,
                    // we need to check is this is computed on oryginal - unwrapped - object.
                    if (orygObj && objKey) {
                        if (ko.isComputed(orygObj[objKey]) || ko.isPureComputed(orygObj[objKey])) {
                            return "function";
                        }
                        if (objKey === "priceTag" || objKey === "priceCurrency", objKey === "options", objKey === "ui") {
                            return "function";
                        }
                    }
                    return ({}).toString.call(obj).match(/\s([a-z|A-Z]+)/)[1].toLowerCase();
                };
            }(self));

            /**
             * Find and return an object from given array,
             * by using property of obj that ends
             * with one of three variants of "ID" string.
             * Ignores those that start with one of two variants of "FK" string.
             * As it might denote Foreign Key that can change in workflow.
             *
             * It will not change the original array.
             *
             * @private
             * @param  {Object}     obj     - Used as sample for searching.
             * @param  {Object[]}   arr     - Array on which searching will be performed.
             * @return {Object}     Object with same xxxID as obj param.
             */
            self.getBySample = function (obj, arr) {
                return arr.find(Object.select(obj, self.matchingCriteria));
            };

            /**
             * Find an object, removes it from given array and returns it.
             *
             * This method will change the array! Use {@link getBySample} for a non-destructive alias.
             *
             * @private
             * @param  {Object}     obj     - Used as sample for searching.
             * @param  {Object[]}   arr     - Array on which searching will be performed.
             * @return {Object}     Object with same xxxID as obj param.
             */
            self.pullBySample = function (obj, arr) {
                var found = self.getBySample.apply(null, arguments);

                arr.remove(found);

                return found;
            };

            /**
             * Represents all changed values from target object.
             * If there are nested objects, objets will be grouped by their types,
             * so it's important to create objects that have yourObject.constructor.name specified.
             *
             * @typedef  {Object}   Dirtiness
             * @property {Object[]} entityType  - Named after objects type, is an array of objects with their xxxID property,
             *                                    and other changed properties. If oryginal object wasn't changed,
             *                                    it won't be added to object of this type at all.
             *
             * @example
             * {
             *   step: [
             *      {stepID: 0, stepNr: 1, stepDescription: "New description"},
             *      {stepID: 3, stepNr: 2, stepDescription: "Lorem ipsum"},
             *      {stepID: 7, stepNr: 3}
             *   ],
             *   offer: [
             *      {offerID: 0, name: "Tooth extraction", imageName: "tooth_extraction", quantity: 2, price: 450},
             *      {offerID: 3, price: 750},
             *      {offerID: 8, price: 900}
             *   ]
             * }
             *
             */

            /**
             * Changed objects grouped by their types.
             *
             * @private
             * @type {Dirtiness}
             */
            self.objectOfDirties = {};

            /**
             * Objects that has been moved between arrays,
             * and needs to be checked between its dirty or clean version.
             * Thus it may contain dirty as well as clean object.
             *
             * @private
             * @type {Array}
             */
            self.movedInObjects = [];

            /**
             * Objects that has been moved into arrays,
             * and needs to be checked between its dirty or clean version.
             * Thus it may contain dirty as well as clean object.
             *
             * @private
             * @type {Array}
             */
            self.movedOutObjects = [];

            /**
             * Returns the "dirty" observables.
             * @note getDirty it is designed to be a function and not an observable to prevent the
             * overhead of constant evaluation.
             *
             * @public
             * @returns {Array} - An array populated with references to "dirty" observables.
             */
            self.getDirty = function (dirtyData, cleanData) {
                /**
                 * Type of added object.
                 * @type {string}
                 */
                var valueType = "";

                /**
                 * @todo Extract this function outside getDirty and make it pure comparison function.
                 */
                function compareAndAddToObjectOfDirties (dirtyDataToCompare, cleanDataToCompare) {
                    var objectTemp = {},
                        unwrapDrtTrgt   = dirtyDataToCompare || self.simplify(target, {level: Level.DEEP}),
                        unwrapClnTrgt   = cleanDataToCompare || self.simplify(_cleanTarget(), {level: Level.DEEP, orygObject: target}),
                        /**
                         * @todo add RECURSIVE function that executes both args on getDirty.
                         */
                        item = {
                            "array" : function (drTg, clTg) {compareAndAddToObjectOfDirties(drTg, clTg); return false; },
                            "object": function (drTg, clTg) {compareAndAddToObjectOfDirties(drTg, clTg); return false; },
                            "string": function (drTg, clTg) {
                                if (drTg !== undefined && clTg !== undefined) {
                                    return (drTg.toNumber() || drTg) !== (clTg.toNumber() || clTg);
                                }

                                return true;
                            },
                            "number": function (drTg, clTg) {
                                if (drTg !== undefined && clTg !== undefined) {
                                    return drTg.toNumber() !== clTg.toNumber();
                                }

                                return true;

                            }
                        },
                        resultType = {
                            array: function (arrayOfObjects, cleanArrayOfObjects) {
                                /**
                                 * Data type of forEach item value.
                                 * @type {string}
                                 */
                                var valueType,
                                    /**
                                     * An object that still remains in array.
                                     * @type {Object}
                                     */
                                    stillInArray = {},
                                    /**
                                     * An object that once has been added to an movedObjects array.
                                     * @type {Object}
                                     */
                                    movedObject = {},
                                    /**
                                     * Array of objects that has been moved into arrayOfObjects.
                                     * Elements comes from arrayOfObjects.
                                     * @type {Object[]}
                                     */
                                    movedIn = arrayOfObjects.subtract(cleanArrayOfObjects),
                                    /**
                                     * Array of objects that has been moved out of arrayOfObjects.
                                     * Elements comes from cleanArrayOfObjects.
                                     * @type {Object[]}
                                     */
                                    movedOut = typeof cleanArrayOfObjects !== "undefined" ? cleanArrayOfObjects.subtract(arrayOfObjects) : {},
                                    /**
                                     * Array of objects that has not came from other array.
                                     * Elements comes from cleanArrayOfObjects.
                                     * @type {Object[]}
                                     */
                                    unmoved;

                                movedIn.forEach(function (value) {
                                    valueType = self.toType(value);

                                    if (valueType === "object") {
                                        movedObject = self.pullBySample(value, self.movedOutObjects);
                                        if (movedObject) {
                                            item[valueType](value, movedObject);
                                        } else {
                                            self.movedInObjects.add(value);
                                        }
                                        movedObject = undefined;

                                        // Remove object that is already or will be checked.
                                        if (unmoved) {
                                            unmoved.remove(value);
                                        } else {
                                            unmoved = arrayOfObjects.exclude(value);
                                        }
                                    }
                                });
                                movedOut.forEach(function (value) {
                                    valueType = self.toType(value);

                                    if (valueType === "object") {
                                        movedObject = self.pullBySample(value, self.movedInObjects);
                                        if (movedObject) {
                                            item[valueType](movedObject, value);

                                        } else {
                                            self.movedOutObjects.add(value);
                                        }
                                        movedObject = undefined;
                                    }
                                });

                                // In case when nothing was added to an array.
                                unmoved = unmoved || arrayOfObjects;

                                unmoved.forEach(function (value) {
                                    valueType = self.toType(value);

                                    if (valueType === "object") {

                                        stillInArray = self.getBySample(value, cleanArrayOfObjects);

                                        /**
                                         * If object is still in array (despite its position),
                                         * compare value with its clean version stillInArray.
                                         */
                                        if (stillInArray) {
                                            item[valueType](value, stillInArray);
                                        }

                                    }
                                });
                            },
                            object: function (dirtyTarget, cleanTarget) {
                                var objectType = cleanTarget.type || "noName",
                                    isValueChanged,
                                    objectsID = {},
                                    valueType;

                                Object.keys(dirtyTarget, function (key, value) {
                                    valueType = self.toType(value);

                                    if (valueType !== "function") {
                                        isValueChanged = item[valueType](value, cleanTarget[key]);
                                        if (isValueChanged) {
                                            objectTemp[key] = value;
                                        } else if (!Object.size(objectsID) && key.has(self.matchingCriteria)) {
                                            objectsID[key] = value;
                                        }
                                    }
                                });

                                if (Object.size(objectTemp)) {
                                    objectTemp = Object.merge(objectsID, objectTemp);
                                    if (self.objectOfDirties[objectType]) {
                                        self.objectOfDirties[objectType].push(objectTemp);
                                    } else {
                                        self.objectOfDirties[objectType] = [objectTemp];
                                    }
                                }
                            }
                        };

                    resultType[self.toType(unwrapDrtTrgt)](unwrapDrtTrgt, unwrapClnTrgt);
                }

                compareAndAddToObjectOfDirties(dirtyData, cleanData);

                // Merge moved and changed with added.
                self.movedInObjects.forEach(function (value) {
                    valueType = value.type || "noName";

                    if (self.objectOfDirties[valueType]) {
                        self.objectOfDirties[valueType].add(value);
                    } else {
                        self.objectOfDirties[valueType] = [value];
                    }

                });

                return self.objectOfDirties;
            };

            /**
             * Tears object from observable wrappers, and depending
             * on choosed level returns modified passed object.
             *
             * This is useful in following scenarios:
             *
             * @private
             * @param {(Object|Object[])}   data                - Data which will be simplified.
             * @param {Object}              [options]           - Additional options.
             * @param {Level}               options.level       - Level of simplification.
             * @param {(Object|Object[])}   options.orygData    - Raw data for comparison purpouse.
             *
             * @returns {(Object|Object[])} Status of the {@link isDirty} flag.
             */
            self.simplify = function (data, options) {
                // if (!(typeof data === "object" || typeof data === "array")) {
                //     throw new Error(typeof data + ": type is unsupported!");
                // }
                options = Object.isObject(options) ? options :  Object.isNumber(options) ? {level: options} : {};

                var result,
                    level = options.level || self.level,
                    unwrapped = (typeof ko.toJS(data) === "string") ? JSON.parse(data) : ko.toJS(data),
                    oryginal = options.orygObject || data,
                    /**
                     * @todo add DEFAULT function that returns argument.
                     * @todo add STRING function that executes both args on getDirty.
                     * @todo add RECURSIVE function that executes argument on simplify.
                     */
                    item = {
                        0: {
                            "array" : function () {return "Array"; },
                            "object": function () {return "Object"; },
                            "string": function (arg) {return arg.toNumber() || arg; },
                            "number": function (arg) {return arg; }
                        },
                        1: {
                            "array" : function (arg) {return self.simplify(arg, {level: level}); },
                            "object": function (arg) {return arg[Object.keys(arg).find(self.param)]; },
                            "string": function (arg) {return arg.toNumber() || arg; },
                            "number": function (arg) {return arg; }
                        },
                        2: {
                            "array" : function (arg) {return self.simplify(arg, {level: level}); },
                            "object": function (arg) {return self.simplify(arg, {level: level}); },
                            "string": function (arg) {return arg.toNumber() || arg; },
                            "number": function (arg) {return arg; }
                        }
                    },
                    resultType = {
                        array: function (arrayOfData) {
                            var valueType;
                            result = [];

                            arrayOfData.forEach(function (value) {
                                valueType = self.toType(value);

                                if (valueType !== "function") {
                                    result.push(item[level][valueType](value));
                                }
                            });
                        },
                        object: function (objectValue) {
                            var valueType;
                            result = {};

                            Object.keys(objectValue, function (key, value) {
                                valueType = self.toType(value, oryginal, key);

                                if ((value !== undefined && value !== null) && (valueType !== "function" && valueType !== "boolean")) {
                                    result[key] = item[level][valueType](value);
                                }
                            });
                        }
                    };

                resultType[self.toType(unwrapped)](unwrapped);

                return result;
            };

            /**
             * Returns the current "dirty" status (true for dirty, false for "clean").
             *
             * @public
             * returns {Boolean} The status of the "isDirty" flag.
             */
            self.isDirty = ko.pureComputed(function () {
                console.debug("caller is ", arguments.callee.caller.toString());
                console.debug("target", target);
                console.debug("clean target", JSON.parse(_cleanTarget()));
                window.dirtyTarget = target;
                /***
                 * @note This comparison can take less than a millisecond vs. iterating over all keys.
                 * Comparing objects in this manner is not always as accurate as can be, but it is
                 * good enough for this case use.
                 */
                var simplifiedTarget       = self.simplify(target),
                // Passing target as second parameter gives us ability
                // to check untouched object, for computed and pureComputed functions.
                // JSON file gets values of the functions, if any.
                    simplifiedCleanTarget  = self.simplify(_cleanTarget(), {orygObject: target});

                console.debug("simplifiedTarget", simplifiedTarget);
                console.debug("simplifiedCleanTarget", simplifiedCleanTarget);

                return _startsDirty() || !Object.equal(simplifiedTarget, simplifiedCleanTarget);

            });

            /**
             * Restores the values to its initial state.
             *
             * @public
             * @todo Improve algorithm
             */
            self.restore = function () {
                var unwrappedDirtyTarget = ko.toJS(target),
                    /*** @note _cleanTarget is stringyfied JSON, it needs to be parsed. */
                    unwrappedCleanTarget = JSON.parse(_cleanTarget()),
                    key;

                /**
                 * @note While it is more efficient to loop trhu the "clean" object, JSON.stringify(used by ko.toJSON) might have removed
                 * properties that need to be reset (most likely to undefined) in the "dirty" target.
                 */
                for (key in unwrappedDirtyTarget) {
                    /***
                     * @important An observable can hold multiple types of data, an strict comparison would not
                     * be enough since diferent instances of objects holding the same values wouldn"t be equal.
                     * As example lodash"s isEqual method is just over 150 lines. A compromise in terms of
                     * performance and is made. The other alternative is to set every value on the view model (or segment)
                     * passed to the isDirty plugin.
                     *
                     * @see https://github.com/lodash/lodash/blob/master/lodash.js#L2113
                     * @see http://ecma-international.org/ecma-262/5.1/#sec-11.9.6
                     * @see http://ecma-international.org/ecma-262/5.1/#sec-9.12
                     */
                    if (unwrappedDirtyTarget.hasOwnProperty(key) &&
                        ko.isObservable(target[key]) &&
                        !ko.isComputed(target[key]) &&
                        JSON.stringify(unwrappedCleanTarget[key]) !== JSON.stringify(unwrappedDirtyTarget[key])) {

                        target[key](unwrappedCleanTarget[key]);
                    }
                }

                /*** @note We do not re-set the _startsDirty property, as it is not a reset flag method. */
            };

            /**
             * Resets the flag to false so that the current state becomes "clean".
             * @public
             */
            self.resetFlag = function () {
                _startsDirty(false);
                _cleanTarget(ko.toJSON(target));
            };

            return self;
        };

        return status;
    };

    /***
     * Attach properties to the exports object to define the exported module properties.
     * @todo Extract logic to UMD packager.
     * @see https://github.com/bebraw/grunt-umd
     */
    exports.DirtyFlag = DirtyFlag;

    /***
     * Roughly equivalent to _.merge, will attaches the DirtyFlag function to the exports object.
     * @see https://github.com/knockout/knockout/blob/master/src/utils.js#L10
     */
    ko.utils.extend(ko, exports);

    return exports;
}));
